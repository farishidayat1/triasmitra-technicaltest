<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        } else {
            return view('auth.login');
        }
    }

    public function login(Request $request)
    {
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (Auth::attempt($data))
        {
            return redirect()->route('dashboard');
        } else {
            Session::flash('error_message', 'Email and Password is Wrong!');
            return redirect()->route('login.form');
        }    
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.form');
    }
}
