<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'DESC')->where('name', '!=', 'admin')->get();

        return view('app.user.index',[
            'activePage' => 'Users',
            'users' => $users
        ]);
    }

    public function create() 
    {
        $getLocation = Http::withToken(env('API_TOKEN_AMS'))->get('https://ams.karyaoptima.com/api/public/get-location');
        $locations = [];
        if($getLocation->successful()) {
            $locations = $getLocation->object()->result->data;
        }

        $getRole = Http::withToken(env('API_TOKEN_AMS'))->get('https://ams.karyaoptima.com/api/public/get-role');
        $roles = [];
        if($getRole->successful()) {
            $roles = $getRole->object()->result->data;
        }

        return view('app.user.create', [
            'activePage' => 'Users',
            'locations' => $locations,
            'roles' => $roles,
        ]);
    }

    public function store(Request $request) 
    {
      $this->validate($request, [
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required|email',
        'address' => 'required',
        'gender' => 'required',
        'role' => 'required',
        'location' => 'required',
        // 'password' => 'required',
      ]);

      $explodeRole = explode(',', $request->role);
      $explodeLocation = explode(',', $request->location);

      $user = User::create([
        'name' => $request->name,
        'phone' => $request->phone,
        'email' => $request->email,
        'password' => Hash::make('password'),
        'address' => $request->address,
        'gender' => $request->gender,
        'role_id' => $explodeRole[0],
        'role_name' => $explodeRole[1],
        'location_id' => $explodeLocation[0],
        'location_name' => $explodeLocation[1],
        'status' => 1
      ]);

      return redirect()->route('user.index')->with('success', 'Successfully Created User');
    }

    public function show($id) 
    {
        $user = User::find($id);

        $getLocation = Http::withToken(env('API_TOKEN_AMS'))->get('https://ams.karyaoptima.com/api/public/get-location');
        $locations = [];
        if($getLocation->successful()) {
            $locations = $getLocation->object()->result->data;
        }

        $getRole = Http::withToken(env('API_TOKEN_AMS'))->get('https://ams.karyaoptima.com/api/public/get-role');
        $roles = [];
        if($getRole->successful()) {
            $roles = $getRole->object()->result->data;
        }

        return view('app.user.edit', [
            'activePage' => 'Users',
            'user' => $user,
            'locations' => $locations,
            'roles' => $roles,
        ]);
    }

    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'gender' => 'required',
            'role' => 'required',
            'location' => 'required',
            // 'password' => 'required',
        ]);

        $explodeRole = explode(',', $request->role);
        $explodeLocation = explode(',', $request->location);

        $user = User::find($id)->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make('password'),
            'address' => $request->address,
            'gender' => $request->gender,
            'role_id' => $explodeRole[0],
            'role_name' => $explodeRole[1],
            'location_id' => $explodeLocation[0],
            'location_name' => $explodeLocation[1],
            'status' => 1
        ]);

        // if(! empty($request->password)) {
        //     $user->update([
        //         'password' => Hash::make($request->password)
        //     ]);
        // }

        return redirect()->route('user.index')->with('success', 'Successfully Updated User');
    }

    public function delete($id) 
    {
        User::find($id)->delete();

        return redirect()->back()->with('success', 'Success Deleted User');
    }

    public function approve(Request $request)
    {
        $userIds = explode(',', $request->user_ids);
        User::whereIn('id', $userIds)->update(['status' => 2]);

        return redirect()->back()->with('success', 'Success Approved User');
    }
}
