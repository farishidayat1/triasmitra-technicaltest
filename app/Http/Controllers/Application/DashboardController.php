<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $totalUser = User::where('name', '!=', 'admin');
        $todayUsers = User::where('name', '!=', 'admin');

        $location = $request->location;
        $status = $request->status;
        $gender = $request->gender;

        if($location) {
            $totalUser = $totalUser->where('location_id', $location);
            $todayUsers = $todayUsers->where('location_id', $location);
        }

        if($status) {
            $totalUser = $totalUser->where('status', $status);
            $todayUsers = $todayUsers->where('status', $status);
        }

        if($gender) {
            $totalUser = $totalUser->where('gender', $gender);
            $todayUsers = $todayUsers->where('gender', $gender);
        }

        $totalUser = $totalUser->count();

        $totalUserNotApprove = User::where('name', '!=', 'admin')->where('status', 1)->count();
        $totalUserApprove = User::where('name', '!=', 'admin')->where('status', 2)->count();

        $dateStart = Carbon::now()->startOfDay()->format('Y-m-d H:i:s');
        $dateEnd = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');
        $todayUsers = $todayUsers->whereBetween('created_at', [$dateStart, $dateEnd])->get();
        
        $getLocationSelected = DB::table('users')->groupBy('location_name')->having(DB::raw('count(location_id)'), '>', 1)->pluck('location_name');
        $selected_locations = [];
        foreach($getLocationSelected as $index => $locationName) {
            $selected_locations[$index]['name'] = $locationName;
            $selected_locations[$index]['total'] = User::where('location_name', $locationName)->count();;
        }

        $getLocation = Http::withToken(env('API_TOKEN_AMS'))->get('https://ams.karyaoptima.com/api/public/get-location');
        $locations = [];
        if($getLocation->successful()) {
            $locations = $getLocation->object()->result->data;
        }

        return view('app.dashboard', [
            'activePage' => 'Dashboard',
            'total_user' => $totalUser,
            'total_user_not_approve' => $totalUserNotApprove,
            'total_user_approve' => $totalUserApprove,
            'today_users' => $todayUsers,
            'selected_locations' => $selected_locations,
            'locations' => $locations,
            'qLocation' => $location,
            'qGender' => $gender,
            'qStatus' => $status,
        ]);
    }
}
