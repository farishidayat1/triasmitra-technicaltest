@extends('app.layout.app')
<style>
    select {
        
    }
</style>
@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.layout.partials.navbar')
    <div class="container-fluid py-4">
        <div class="row col-md-12">
            <form method="GET" action="{{ route('dashboard') }}" id="form-dash">
                <div class="d-flex justify-content-end" style="gap: 10px;">
                    <div class="form-group">
                        <select class="form-select" name="location" style="width: 140px !important;" onchange="submitForm()">
                            <option value="">Select Location</option>
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}" @if($qLocation == $location->id) selected @endif>{{ $location->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-select" name="status" style="width: 125px !important;" onchange="submitForm()">
                            <option value="">Select Status</option>
                            <option value="2" @if($qStatus == 2) selected @endif>Approve</option>
                            <option value="1" @if($qStatus == 1) selected @endif>Not Approve</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-select" name="gender" style="width: 135px !important;" onchange="submitForm()">
                            <option value="">Select Gender</option>
                            <option value="Male" @if($qGender == 'Male') selected @endif>Male</option>
                            <option value="Female" @if($qGender == 'Female') selected @endif>Female</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
            <div class="card-body p-3">
                <div class="row">
                <div class="col-8">
                    <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Users</p>
                    <h5 class="font-weight-bolder">
                        {{ $total_user }}
                    </h5>
                    {{-- <p class="mb-0">
                        <span class="text-success text-sm font-weight-bolder">+55%</span>
                        since yesterday
                    </p> --}}
                    </div>
                </div>
                <div class="col-4 text-end">
                    <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                    <i class="ni ni-circle-08 text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
            <div class="card-body p-3">
                <div class="row">
                <div class="col-8">
                    <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Users Not Approve</p>
                    <h5 class="font-weight-bolder">
                        {{ $total_user_not_approve }}
                    </h5>
                    {{-- <p class="mb-0">
                        <span class="text-success text-sm font-weight-bolder">+3%</span>
                        since last week
                    </p> --}}
                    </div>
                </div>
                <div class="col-4 text-end">
                    <div class="icon icon-shape bg-gradient-danger shadow-danger text-center rounded-circle">
                    <i class="ni ni-single-02 text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
            <div class="card-body p-3">
                <div class="row">
                <div class="col-8">
                    <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Users Approve</p>
                    <h5 class="font-weight-bolder">
                        {{ $total_user_approve }}
                    </h5>
                    {{-- <p class="mb-0">
                        <span class="text-danger text-sm font-weight-bolder">-2%</span>
                        since last quarter
                    </p> --}}
                    </div>
                </div>
                <div class="col-4 text-end">
                    <div class="icon icon-shape bg-gradient-success shadow-success text-center rounded-circle">
                    <i class="ni ni-single-02 text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="row mt-4">
        <div class="col-lg-7 mb-lg-0 mb-4">
            <div class="card ">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                <h6 class="mb-2">Users Today</h6>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center ">
                    <tbody>
                        @foreach($today_users as $user)
                            <tr>
                                <td class="w-30">
                                    <div class="d-flex px-2 py-1 align-items-center">
                                        <div class="ms-4">
                                            <p class="text-xs font-weight-bold mb-0">Name:</p>
                                            <h6 class="text-sm mb-0">{{ $user->name }}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                    <p class="text-xs font-weight-bold mb-0">Email:</p>
                                    <h6 class="text-sm mb-0">{{ $user->email }}</h6>
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center">
                                    <p class="text-xs font-weight-bold mb-0">Role:</p>
                                    <h6 class="text-sm mb-0">{{ $user->role_name }}</h6>
                                    </div>
                                </td>
                                <td class="align-middle text-sm">
                                    <div class="col text-center">
                                    <p class="text-xs font-weight-bold mb-0">Location:</p>
                                    <h6 class="text-sm mb-0">{{ $user->location_name }}</h6>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="card">
            <div class="card-header pb-0 p-3">
                <h6 class="mb-0">Location User</h6>
            </div>
            <div class="card-body p-3">
                <ul class="list-group">
                    @foreach($selected_locations as $location)
                        <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                            <div class="d-flex align-items-center">
                                <div class="icon icon-shape icon-sm me-3 bg-gradient-dark shadow text-center">
                                    <i class="ni ni-pin-3 text-white opacity-10"></i>
                                </div>
                                <div class="d-flex flex-column">
                                    <h6 class="mb-1 text-dark text-sm">{{ $location['name'] }}</h6>
                                    <span class="text-xs">{{ $location['total'] }}</span>
                                </div>
                            </div>
                            {{-- <div class="d-flex">
                                <button class="btn btn-link btn-icon-only btn-rounded btn-sm text-dark icon-move-right my-auto"><i class="ni ni-bold-right" aria-hidden="true"></i></button>
                            </div> --}}
                        </li>
                    @endforeach
                </ul>
            </div>
            </div>
        </div>
        </div>
    </div>
</main>

<script>
    function submitForm() {
        $('#form-dash').submit()
    }
  </script>
@endsection