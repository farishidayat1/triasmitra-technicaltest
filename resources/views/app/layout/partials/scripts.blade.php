<!--   Core JS Files   -->
<script src="{{ asset('asset-template/js/core/popper.min.js') }}"></script>
<script src="{{ asset('asset-template/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('asset-template/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('asset-template/js/plugins/smooth-scrollbar.min.js') }}"></script>
<script>
var win = navigator.platform.indexOf('Win') > -1;
if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
    damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
}
</script>
<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('asset-template/js/argon-dashboard.min.js?v=2.0.4') }}"></script>
<script src="{{ asset('asset-template/js/plugins/smooth-scrollbar.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>

@yield('scripts')