@extends('app.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.layout.partials.navbar')
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
              <div class="d-flex justify-content-between">
                <form class="d-none" method="POST" action="{{ route('user.approve') }}" id="form-approve">
                  @csrf
                </form>
                <a onclick="submitApprove()" class="btn btn-primary">Approve User</a>
                <a href="{{ route('user.create') }}" class="btn btn-outline-primary">Create User</a>
              </div>
              <h6>Users table</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center justify-content-center mb-0">
                  <thead>
                    <tr>
                      <th style="width: 20px;"></th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Phone</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Gender</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Address</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Role</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Location</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>
                          @if($user->status == 1)
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" name="user_ids[]" value="{{ $user->id }}">
                            </div>
                          @endif
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm" style="margin-left: 10px;">{{ $user->name }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm" style="margin-left: 10px;">{{ $user->phone }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->email }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->gender }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->address }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->role_name }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->location_name }}</h6>
                        </td>
                        <td class="align-middle d-flex flex-wrap">
                          <a style="padding:4px 0;" class="btn btn-link text-secondary mb-0" href="{{ route('user.show', $user->id) }}">
                            <i class="fas fa-eye"></i>
                          </a>
                          @if(auth()->user()->id != $user->id)
                            <form id="form-delete-{{ $user->id }}" method="POST" action="{{ route('user.delete', $user->id) }}">
                              @csrf
                              @method('DELETE')
                              <a onclick="confirmationDelete({{ $user->id }})" style="padding:0" class="btn btn-link text-secondary mb-0 mx-2">
                                <i class="fas fa-trash"></i>
                              </a>
                            </form>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <script>
    function submitApprove() {
      let userIds = [];
      $(':checkbox:checked').each(function(i){
        userIds[i] = $(this).val();
      });

      if(userIds.length > 0) {
        const alert = confirm('Are you sure to approve this users ?')
        if(alert) {
          const input = $("<input>").attr("type", "hidden").attr("name", "user_ids").val(userIds);
          $('#form-approve').append(input);
          $('#form-approve').submit()
        }
      }
    }

    function confirmationDelete(id) {
      const alert = confirm('Are you sure to delete this user ?')
      if(alert) {
        $("#form-delete-"+id).submit();
      }
    }
  </script>
@endsection