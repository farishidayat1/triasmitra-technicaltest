@extends('app.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.layout.partials.navbar')
   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<i class="fas fa-plus"></i>
						Edit {{ $user->name }}
					</div>
					<div class="card-body">
						@include('auth.layout.partials.alert-message')
						<form method="POST" action="{{ route('user.update', $user->id) }}">
							@method('PUT')
							@csrf
							<div class="form-group">
								<label>Name</label>
								<input type="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $user->name }}">
								@error('name')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Phone</label>
								<input type="text" maxlength="11" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ $user->phone }}">
								@error('phone')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $user->email }}">
								@error('email')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea type="text" name="address" rows="5" class="form-control @error('address') is-invalid @enderror">{{ $user->address }}</textarea> 
								@error('address')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Gender</label>
								<select class="form-select @error('gender') is-invalid @enderror" name="gender">
									<option value="">Select Gender</option>
									<option value="Male" @if($user->gender == 'Male') selected @endif>Male</option>
									<option value="Female" @if($user->gender == 'Female') selected @endif>Female</option>
								</select>
								@error('gender')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Role</label>
								<select class="form-select @error('role') is-invalid @enderror" name="role">
									<option value="">Select Role</option>
									@foreach($roles as $role)
										<option value="{{ $role->id.','.$role->name }}" @if($user->role_id == $role->id) selected @endif>{{ $role->name }}</option>
									@endforeach
								</select>
								@error('role')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Location</label>
								<select class="form-select @error('location') is-invalid @enderror" name="location">
									<option value="">Select Location</option>
									@foreach($locations as $location)
										<option value="{{ $location->id.','.$location->name }}" @if($user->location_id == $location->id) selected @endif>{{ $location->name }}</option>
									@endforeach
								</select>
								@error('location')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							{{-- <div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control @error('password') is-invalid @enderror" >
								 <small class="form-text text-muted ml-3 mt-2">Leave password blank if you don't want to change the password.</small>
								@error('password')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div> --}}
							<button class="btn btn-primary" type="submit">Update User</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@endsection