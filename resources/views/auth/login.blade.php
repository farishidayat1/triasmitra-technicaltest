@extends('auth.layout.app')

@section('contents')
@include('auth.layout.partials.alert-message')
<form action="{{ route('login') }}" method="POST">
    @csrf
    <div class="mb-3">
      <input type="email" class="form-control" placeholder="Email" aria-label="Email" name="email">
    </div>
    <div class="mb-3">
      <input type="password" class="form-control" placeholder="Password" aria-label="Password" name="password">
    </div>
    <div class="text-center">
      <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign in</button>
    </div>
</form>   
@endsection